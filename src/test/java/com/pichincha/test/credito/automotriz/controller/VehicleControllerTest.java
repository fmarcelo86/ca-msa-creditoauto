package com.pichincha.test.credito.automotriz.controller;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.controller.VehicleController;
import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.VehicleService;
import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class VehicleControllerTest {

  @InjectMocks
  private VehicleController vehicleController;
  @Mock
  private VehicleService vehicleService;

  @DisplayName("Test method return to list of vehicle")
  @Test
  void shouldReturnListOfVehicle() {
    List<VehicleDto> vehiclesDto = List.of(VehicleDto.builder().build(),
        VehicleDto.builder().build());

    when(vehicleService.getAll()).thenReturn(vehiclesDto);
    ResponseEntity<List<VehicleDto>> responseEntity = vehicleController.getAll();

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertEquals(2, responseEntity.getBody().size());
    verify(vehicleService, times(1)).getAll();
  }

  @DisplayName("Test method return one vehicle")
  @Test
  void shouldReturnOneByIdVehicle() {
    VehicleDto vehicleDto = VehicleDto.builder().build();

    when(vehicleService.getById(1L)).thenReturn(vehicleDto);
    ResponseEntity<Object> responseEntity = vehicleController.getById(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(vehicleService, times(1)).getById(1L);
  }

  @DisplayName("Test method to create new vehicle")
  @Test
  void givenVehicleWhenCreateVehicleThenCreateNewVehicle() {
    VehicleDto vehicleDto = VehicleDto.builder().build();

    when(vehicleService.save(vehicleDto)).thenReturn(vehicleDto);
    ResponseEntity<VehicleDto> responseEntity = vehicleController.create(vehicleDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    assertThat(responseEntity.getBody()).isEqualTo(vehicleDto);
    verify(vehicleService, times(1)).save(vehicleDto);
  }

  @DisplayName("Test method to update one vehicle")
  @Test
  void givenVehicleWhenUpdateVehicleThenUpdateVehicle() {
    VehicleDto vehicleDto = VehicleDto.builder().build();

    when(vehicleService.update(vehicleDto)).thenReturn(vehicleDto);
    ResponseEntity<Object> responseEntity = vehicleController.update(vehicleDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(vehicleDto);
    verify(vehicleService, times(1)).update(vehicleDto);
  }

  @DisplayName("Test method to delete one vehicle")
  @Test
  void shouldDeleteOneByIdVehicle() {
    doNothing().when(vehicleService).delete(1L);
    ResponseEntity<ErrorResponse> responseEntity = vehicleController.delete(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(vehicleService, times(1)).delete(1L);
  }
}
