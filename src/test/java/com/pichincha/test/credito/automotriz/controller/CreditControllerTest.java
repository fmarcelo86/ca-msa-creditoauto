package com.pichincha.test.credito.automotriz.controller;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.controller.CreditController;
import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.CreditService;
import com.pichincha.credito.automotriz.service.dto.CreditDto;
import com.pichincha.credito.automotriz.service.dto.CreditRequestDto;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class CreditControllerTest {

  @InjectMocks
  private CreditController creditController;
  @Mock
  private CreditService creditService;

  @DisplayName("Test method return to list of credit")
  @Test
  void shouldReturnListOfCredit() {
    List<CreditDto> creditsDto = List.of(CreditDto.builder().build(),
        CreditDto.builder().build());

    when(creditService.getAll()).thenReturn(creditsDto);
    ResponseEntity<List<CreditDto>> responseEntity = creditController.getAll();

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertEquals(2, responseEntity.getBody().size());
    verify(creditService, times(1)).getAll();
  }

  @DisplayName("Test method return one credit")
  @Test
  void shouldReturnOneByIdCredit() {
    CreditDto creditDto = CreditDto.builder().build();

    when(creditService.getById(1L)).thenReturn(creditDto);
    ResponseEntity<Object> responseEntity = creditController.getById(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(creditService, times(1)).getById(1L);
  }

  @DisplayName("Test method to create new credit")
  @Test
  void givenCreditWhenCreateCreditThenCreateNewCredit() {
    CreditDto creditDto = CreditDto.builder().build();

    when(creditService.save(creditDto)).thenReturn(creditDto);
    ResponseEntity<Object> responseEntity = creditController.create(creditDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    assertThat(responseEntity.getBody()).isEqualTo(creditDto);
    verify(creditService, times(1)).save(creditDto);
  }

  @DisplayName("Test method to update one credit")
  @Test
  void givenCreditWhenUpdateCreditThenUpdateCredit() {
    CreditDto creditDto = CreditDto.builder().build();

    when(creditService.update(creditDto)).thenReturn(creditDto);
    ResponseEntity<Object> responseEntity = creditController.update(creditDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(creditDto);
    verify(creditService, times(1)).update(creditDto);
  }

  @DisplayName("Test method to update status of credit")
  @Test
  void givenCreditWhenUpdateStatusCreditThenUpdateStatusCredit() {
    CreditRequestDto creditRequestDto = CreditRequestDto.builder().status("DESPACHADA").build();
    CreditDto creditDto = CreditDto.builder().build();

    when(creditService.updateStatus(creditRequestDto)).thenReturn(creditDto);
    ResponseEntity<Object> responseEntity = creditController.update(creditRequestDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(creditDto);
    verify(creditService, times(1)).updateStatus(creditRequestDto);
  }

  @DisplayName("Test method to delete one credit")
  @Test
  void shouldDeleteOneByIdCredit() {
    doNothing().when(creditService).delete(1L);
    ResponseEntity<ErrorResponse> responseEntity = creditController.delete(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(creditService, times(1)).delete(1L);
  }
}
