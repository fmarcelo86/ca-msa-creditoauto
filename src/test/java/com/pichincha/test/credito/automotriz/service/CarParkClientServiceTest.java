package com.pichincha.test.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.domain.CarPark;
import com.pichincha.credito.automotriz.domain.CarParkClient;
import com.pichincha.credito.automotriz.domain.Client;
import com.pichincha.credito.automotriz.repository.CarParkClientRepository;
import com.pichincha.credito.automotriz.service.dto.CarParkClientDto;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import com.pichincha.credito.automotriz.service.dto.ClientDto;
import com.pichincha.credito.automotriz.service.impl.CarParkClientServiceImpl;
import com.pichincha.credito.automotriz.service.mapper.CarParkClientMapper;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CarParkClientServiceTest {
  @InjectMocks
  private CarParkClientServiceImpl carParkClientService;
  @Mock
  private CarParkClientRepository carParkClientRepository;
  @Spy
  private CarParkClientMapper carParkClientMapper = Mappers.getMapper(CarParkClientMapper.class);

  @DisplayName("Test method return to list of car park client")
  @Test
  void shouldReturnListOfCarParkClient() {
    List<CarParkClient> carParkClients = List.of(CarParkClient.builder().build(),
        CarParkClient.builder().build());

    when(carParkClientRepository.findAll()).thenReturn(carParkClients);
    when(carParkClientMapper.toData(carParkClients.get(0))).thenReturn(CarParkClientDto.builder().build());
    List<CarParkClientDto> carParkClientsDto = carParkClientService.getAll();

    assertEquals(2, carParkClientsDto.size());
    verify(carParkClientRepository, times(1)).findAll();
  }

  @DisplayName("Test method return one car park client")
  @Test
  void shouldReturnOneByIdCarParkClient() {
    CarParkClient carParkClient = CarParkClient.builder().build();

    when(carParkClientRepository.findById(1L)).thenReturn(Optional.of(carParkClient));
    when(carParkClientMapper.toData(carParkClient)).thenReturn(CarParkClientDto.builder().build());
    CarParkClientDto carParkClientDto = carParkClientService.getById(1L);

    assertEquals(carParkClientDto, CarParkClientDto.builder().build());
  }

  @DisplayName("Test method to create new car park client")
  @Test
  void givenCarParkClientWhenCreateCarParkClientThenCreateNewCarParkClient() {
    CarParkClient carParkClient = CarParkClient.builder()
        .client(Client.builder().build())
        .carPark(CarPark.builder().build())
        .build();
    CarParkClientDto carParkClientDtoRequest = CarParkClientDto.builder()
        .clientDto(ClientDto.builder().build())
        .carParkDto(CarParkDto.builder().build())
        .build();

    when(carParkClientRepository.save(carParkClient)).thenReturn(carParkClient);
    when(carParkClientMapper.toEntity(carParkClientDtoRequest)).thenReturn(carParkClient);
    when(carParkClientMapper.toData(carParkClient)).thenReturn(carParkClientDtoRequest);
    CarParkClientDto carParkClientDto = carParkClientService.save(carParkClientDtoRequest);

    assertEquals(carParkClientDto, carParkClientDtoRequest);
    verify(carParkClientRepository, times(1)).save(carParkClient);
  }

  @DisplayName("Test method to update one car park client")
  @Test
  void givenCarParkClientWhenUpdateCarParkClientThenUpdateCarParkClient() {
    CarParkClient carParkClient = CarParkClient.builder().id(1L).build();
    CarParkClientDto carParkClientDtoRequest = CarParkClientDto.builder().id(1L).build();

    when(carParkClientRepository.findById(1L)).thenReturn(Optional.of(carParkClient));
    when(carParkClientRepository.save(carParkClient)).thenReturn(carParkClient);
    when(carParkClientMapper.toEntity(carParkClientDtoRequest)).thenReturn(carParkClient);
    when(carParkClientMapper.toData(carParkClient)).thenReturn(carParkClientDtoRequest);
    CarParkClientDto carParkClientDto = carParkClientService.update(carParkClientDtoRequest);

    assertEquals(carParkClientDto, carParkClientDtoRequest);
    verify(carParkClientRepository, times(1)).save(carParkClient);
  }

  @DisplayName("Test method to delete one car park client")
  @Test
  void shouldDeleteOneByIdCarParkClient() {
    CarParkClient carParkClient = CarParkClient.builder().id(1L).build();

    when(carParkClientRepository.findById(1L)).thenReturn(Optional.of(carParkClient));
    doNothing().when(carParkClientRepository).deleteById(1L);
    carParkClientService.delete(1L);

    verify(carParkClientRepository, times(1)).findById(1L);
    verify(carParkClientRepository, times(1)).deleteById(1L);
  }
}
