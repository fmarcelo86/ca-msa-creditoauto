package com.pichincha.test.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.domain.CarPark;
import com.pichincha.credito.automotriz.repository.CarParkRepository;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import com.pichincha.credito.automotriz.service.impl.CarParkServiceImpl;
import com.pichincha.credito.automotriz.service.mapper.CarParkMapper;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CarParkServiceTest {
  @InjectMocks
  private CarParkServiceImpl carParkService;
  @Mock
  private CarParkRepository carParkRepository;
  @Spy
  private CarParkMapper carParkMapper = Mappers.getMapper(CarParkMapper.class);

  @DisplayName("Test method return to list of car park")
  @Test
  void shouldReturnListOfCarPark() {
    List<CarPark> carParks = List.of(CarPark.builder().build(),
        CarPark.builder().build());

    when(carParkRepository.findAll()).thenReturn(carParks);
    when(carParkMapper.toData(carParks.get(0))).thenReturn(CarParkDto.builder().build());
    List<CarParkDto> carParksDto = carParkService.getAll();

    assertEquals(2, carParksDto.size());
    verify(carParkRepository, times(1)).findAll();
  }

  @DisplayName("Test method return one car park")
  @Test
  void shouldReturnOneByIdCarPark() {
    CarPark carPark = CarPark.builder().build();

    when(carParkRepository.findById(1L)).thenReturn(Optional.of(carPark));
    when(carParkMapper.toData(carPark)).thenReturn(CarParkDto.builder().build());
    CarParkDto carParkDto = carParkService.getById(1L);

    assertEquals(carParkDto, CarParkDto.builder().build());
    verify(carParkRepository, times(1)).findById(1L);
    verify(carParkMapper, times(1)).toData(carPark);
  }

  @DisplayName("Test method to create new car park")
  @Test
  void givenCarParkWhenCreateCarParkThenCreateNewCarPark() {
    CarPark carPark = CarPark.builder().build();
    CarParkDto carParkDtoRequest = CarParkDto.builder().build();

    when(carParkRepository.save(carPark)).thenReturn(carPark);
    when(carParkMapper.toEntity(carParkDtoRequest)).thenReturn(carPark);
    when(carParkMapper.toData(carPark)).thenReturn(carParkDtoRequest);
    CarParkDto carParkDto = carParkService.save(carParkDtoRequest);

    assertEquals(carParkDto, carParkDtoRequest);
    verify(carParkRepository, times(1)).save(carPark);
    verify(carParkMapper, times(1)).toEntity(carParkDtoRequest);
    verify(carParkMapper, times(1)).toData(carPark);
  }

  @DisplayName("Test method to update one car park")
  @Test
  void givenCarParkWhenUpdateCarParkThenUpdateCarPark() {
    CarPark carPark = CarPark.builder().id(1L).build();
    CarParkDto carParkDtoRequest = CarParkDto.builder().id(1L).build();

    when(carParkRepository.findById(1L)).thenReturn(Optional.of(carPark));
    when(carParkRepository.save(carPark)).thenReturn(carPark);
    when(carParkMapper.toEntity(carParkDtoRequest)).thenReturn(carPark);
    when(carParkMapper.toData(carPark)).thenReturn(carParkDtoRequest);
    CarParkDto carParkDto = carParkService.update(carParkDtoRequest);

    assertEquals(carParkDto, carParkDtoRequest);
    verify(carParkRepository, times(1)).save(carPark);
    verify(carParkMapper, times(1)).toEntity(carParkDtoRequest);
    verify(carParkMapper, times(1)).toData(carPark);
  }

  @DisplayName("Test method to delete one car park")
  @Test
  void shouldDeleteOneByIdCarPark() {
    CarPark carPark = CarPark.builder().id(1L).build();

    when(carParkRepository.findById(1L)).thenReturn(Optional.of(carPark));
    doNothing().when(carParkRepository).deleteById(1L);
    carParkService.delete(1L);

    verify(carParkRepository, times(1)).findById(1L);
    verify(carParkRepository, times(1)).deleteById(1L);
  }
}
