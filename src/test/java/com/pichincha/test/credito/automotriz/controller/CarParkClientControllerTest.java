package com.pichincha.test.credito.automotriz.controller;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.controller.CarParkClientController;
import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.CarParkClientService;
import com.pichincha.credito.automotriz.service.dto.CarParkClientDto;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class CarParkClientControllerTest {

  @InjectMocks
  private CarParkClientController carParkClientController;
  @Mock
  private CarParkClientService carParkClientService;

  @DisplayName("Test method return to list of carParkClient")
  @Test
  void shouldReturnListOfCarParkClient() {
    List<CarParkClientDto> carParkClientsDto = List.of(CarParkClientDto.builder().build(),
        CarParkClientDto.builder().build());

    when(carParkClientService.getAll()).thenReturn(carParkClientsDto);
    ResponseEntity<List<CarParkClientDto>> responseEntity = carParkClientController.getAll();

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertEquals(2, responseEntity.getBody().size());
    verify(carParkClientService, times(1)).getAll();
  }

  @DisplayName("Test method return one carParkClient")
  @Test
  void shouldReturnOneByIdCarParkClient() {
    CarParkClientDto carParkClientDto = CarParkClientDto.builder().build();

    when(carParkClientService.getById(1L)).thenReturn(carParkClientDto);
    ResponseEntity<Object> responseEntity = carParkClientController.getById(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(carParkClientService, times(1)).getById(1L);
  }

  @DisplayName("Test method to create new carParkClient")
  @Test
  void givenCarParkClientWhenCreateCarParkClientThenCreateNewCarParkClient() {
    CarParkClientDto carParkClientDto = CarParkClientDto.builder().build();

    when(carParkClientService.save(carParkClientDto)).thenReturn(carParkClientDto);
    ResponseEntity<CarParkClientDto> responseEntity = carParkClientController.create(carParkClientDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    assertThat(responseEntity.getBody()).isEqualTo(carParkClientDto);
    verify(carParkClientService, times(1)).save(carParkClientDto);
  }

  @DisplayName("Test method to update one carParkClient")
  @Test
  void givenCarParkClientWhenUpdateCarParkClientThenUpdateCarParkClient() {
    CarParkClientDto carParkClientDto = CarParkClientDto.builder().build();

    when(carParkClientService.update(carParkClientDto)).thenReturn(carParkClientDto);
    ResponseEntity<Object> responseEntity = carParkClientController.update(carParkClientDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(carParkClientDto);
    verify(carParkClientService, times(1)).update(carParkClientDto);
  }

  @DisplayName("Test method to delete one carParkClient")
  @Test
  void shouldDeleteOneByIdCarParkClient() {
    doNothing().when(carParkClientService).delete(1L);
    ResponseEntity<ErrorResponse> responseEntity = carParkClientController.delete(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(carParkClientService, times(1)).delete(1L);
  }
}
