package com.pichincha.test.credito.automotriz.util;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.domain.enums.ErrorMessageEnum;
import com.pichincha.credito.automotriz.exception.ServiceNotFoundException;
import com.pichincha.credito.automotriz.util.GlobalExceptionHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

@ExtendWith(MockitoExtension.class)
class GlobalExceptionHandlerTest {

  @InjectMocks
  private GlobalExceptionHandler exceptionHandler;
  @Mock
  private MethodArgumentNotValidException methodArgumentNotValidException;
  @Mock
  private MethodParameter parameter;
  @Mock
  private BindingResult bindingResult;

  @DisplayName("Test method return error")
  @Test
  void shouldReturnErrorHandleExceptions() {
    ServiceNotFoundException notFoundException = new ServiceNotFoundException(
        ErrorMessageEnum.CAR_PARK_NOT_FOUND);
    ErrorResponse errorResponse = ErrorResponse.builder()
        .code(ErrorMessageEnum.CAR_PARK_NOT_FOUND.getCode())
        .message(ErrorMessageEnum.CAR_PARK_NOT_FOUND.getMessage())
        .build();

    ResponseEntity<ErrorResponse> responseEntity = exceptionHandler.handleExceptions(
        notFoundException);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    assertThat(responseEntity.getBody()).isEqualTo(errorResponse);
  }

  @DisplayName("Test method return error request not valid")
  @Test
  void shouldReturnErrorArgumentNotValid() {
    methodArgumentNotValidException = new MethodArgumentNotValidException(parameter, bindingResult);
    ErrorResponse errorResponse = ErrorResponse.builder()
        .message("[]")
        .build();

    ResponseEntity<ErrorResponse> responseEntity = exceptionHandler.handleExceptions(
        methodArgumentNotValidException);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    assertThat(responseEntity.getBody()).isEqualTo(errorResponse);
  }
}
