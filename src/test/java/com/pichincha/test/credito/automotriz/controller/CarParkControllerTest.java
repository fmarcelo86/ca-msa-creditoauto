package com.pichincha.test.credito.automotriz.controller;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.controller.CarParkController;
import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.CarParkService;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class CarParkControllerTest {

  @InjectMocks
  private CarParkController carParkController;
  @Mock
  private CarParkService carParkService;

  @DisplayName("Test method return to list of car park")
  @Test
  void shouldReturnListOfCarPark() {
    List<CarParkDto> carParksDto = List.of(CarParkDto.builder().build(),
        CarParkDto.builder().build());

    when(carParkService.getAll()).thenReturn(carParksDto);
    ResponseEntity<List<CarParkDto>> responseEntity = carParkController.getAll();

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertEquals(2, responseEntity.getBody().size());
    verify(carParkService, times(1)).getAll();
  }

  @DisplayName("Test method return one car park")
  @Test
  void shouldReturnOneByIdCarPark() {
    CarParkDto carParkDto = CarParkDto.builder().build();

    when(carParkService.getById(1L)).thenReturn(carParkDto);
    ResponseEntity<Object> responseEntity = carParkController.getById(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(carParkService, times(1)).getById(1L);
  }

  @DisplayName("Test method to create new car park")
  @Test
  void givenCarParkWhenCreateCarParkThenCreateNewCarPark() {
    CarParkDto carParkDto = CarParkDto.builder().build();

    when(carParkService.save(carParkDto)).thenReturn(carParkDto);
    ResponseEntity<CarParkDto> responseEntity = carParkController.create(carParkDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    assertThat(responseEntity.getBody()).isEqualTo(carParkDto);
    verify(carParkService, times(1)).save(carParkDto);
  }

  @DisplayName("Test method to update one car park")
  @Test
  void givenCarParkWhenUpdateCarParkThenUpdateCarPark() {
    CarParkDto carParkDto = CarParkDto.builder().build();

    when(carParkService.update(carParkDto)).thenReturn(carParkDto);
    ResponseEntity<Object> responseEntity = carParkController.update(carParkDto);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(carParkDto);
    verify(carParkService, times(1)).update(carParkDto);
  }

  @DisplayName("Test method to delete one car park")
  @Test
  void shouldDeleteOneByIdCarPark() {
    doNothing().when(carParkService).delete(1L);
    ResponseEntity<ErrorResponse> responseEntity = carParkController.delete(1);

    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    verify(carParkService, times(1)).delete(1L);
  }
}
