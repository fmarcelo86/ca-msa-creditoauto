package com.pichincha.test.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.domain.CarPark;
import com.pichincha.credito.automotriz.domain.CarParkClient;
import com.pichincha.credito.automotriz.domain.Client;
import com.pichincha.credito.automotriz.domain.Credit;
import com.pichincha.credito.automotriz.domain.Seller;
import com.pichincha.credito.automotriz.domain.Vehicle;
import com.pichincha.credito.automotriz.domain.enums.CreditStatusEnum;
import com.pichincha.credito.automotriz.repository.CarParkClientRepository;
import com.pichincha.credito.automotriz.repository.ClientRepository;
import com.pichincha.credito.automotriz.repository.CreditRepository;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import com.pichincha.credito.automotriz.service.dto.ClientDto;
import com.pichincha.credito.automotriz.service.dto.CreditDto;
import com.pichincha.credito.automotriz.service.dto.CreditRequestDto;
import com.pichincha.credito.automotriz.service.dto.SellerDto;
import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import com.pichincha.credito.automotriz.service.impl.CreditServiceImpl;
import com.pichincha.credito.automotriz.service.mapper.CreditMapper;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CreditServiceTest {

  @InjectMocks
  private CreditServiceImpl creditService;
  @Mock
  private CreditRepository creditRepository;
  @Mock
  private ClientRepository clientRepository;
  @Mock
  private CarParkClientRepository carParkClientRepository;
  @Spy
  private CreditMapper creditMapper = Mappers.getMapper(CreditMapper.class);

  @DisplayName("Test method return to list of credit")
  @Test
  void shouldReturnListOfCredit() {
    List<Credit> credits = List.of(Credit.builder().build(),
        Credit.builder().build());

    when(creditRepository.findAll()).thenReturn(credits);
    when(creditMapper.toData(credits.get(0))).thenReturn(CreditDto.builder().build());
    List<CreditDto> creditsDto = creditService.getAll();

    assertEquals(2, creditsDto.size());
    verify(creditRepository, times(1)).findAll();
  }

  @DisplayName("Test method return one credit")
  @Test
  void shouldReturnOneByIdCredit() {
    Credit credit = Credit.builder().build();

    when(creditRepository.findById(1L)).thenReturn(Optional.of(credit));
    when(creditMapper.toData(credit)).thenReturn(CreditDto.builder().build());
    CreditDto creditDto = creditService.getById(1L);

    assertEquals(creditDto, CreditDto.builder().build());
  }

  @DisplayName("Test method to create new credit")
  @Test
  void givenCreditWhenCreateCreditThenCreateNewCredit() {
    Credit credit = Credit.builder()
        .client(Client.builder().build())
        .vehicle(Vehicle.builder().build())
        .carPark(CarPark.builder().build())
        .seller(Seller.builder().build())
        .build();
    CreditDto creditDtoRequest = CreditDto.builder()
        .clientDto(ClientDto.builder().build())
        .vehicleDto(VehicleDto.builder().build())
        .carParkDto(CarParkDto.builder().build())
        .sellerDto(SellerDto.builder().build())
        .build();

    when(clientRepository.findById(creditDtoRequest.getClientDto().getId())).thenReturn(
        Optional.of(Client.builder().subjectCredit(true).build()));
    when(creditRepository.existsByClientIdAndCreatedDateAndStatus(any(), any(), any()))
        .thenReturn(Optional.of(false));
    when(carParkClientRepository.save(any())).thenReturn(CarParkClient.builder().build());
    when(creditRepository.existsByVehicleIdAndStatus(creditDtoRequest.getVehicleDto().getId(),
        CreditStatusEnum.REGISTRADA)).thenReturn(Optional.of(false));
    when(creditRepository.save(credit)).thenReturn(credit);
    when(creditMapper.toEntity(creditDtoRequest)).thenReturn(credit);
    when(creditMapper.toData(credit)).thenReturn(creditDtoRequest);

    CreditDto creditDto = creditService.save(creditDtoRequest);

    assertEquals(creditDto, creditDtoRequest);
    verify(creditRepository, times(1)).save(credit);
  }

  @DisplayName("Test method to update one credit")
  @Test
  void givenCreditWhenUpdateCreditThenUpdateCredit() {
    Credit credit = Credit.builder().id(1L).build();
    CreditDto creditDtoRequest = CreditDto.builder().id(1L).build();

    when(creditRepository.findById(1L)).thenReturn(Optional.of(credit));
    when(creditRepository.save(credit)).thenReturn(credit);
    when(creditMapper.toEntity(creditDtoRequest)).thenReturn(credit);
    when(creditMapper.toData(credit)).thenReturn(creditDtoRequest);
    CreditDto creditDto = creditService.update(creditDtoRequest);

    assertEquals(creditDto, creditDtoRequest);
    verify(creditRepository, times(1)).save(credit);
  }

  @DisplayName("Test method to update status of credit")
  @Test
  void givenCreditWhenUpdateStatusCreditThenUpdateStatusCredit() {
    Credit credit = Credit.builder().id(1L).build();
    CreditRequestDto creditRequestDto = CreditRequestDto.builder()
        .id(1L).status("DESPACHADA").build();
    CreditDto creditDto = CreditDto.builder().id(1L).build();

    when(creditRepository.findById(1L)).thenReturn(Optional.of(credit));
    when(creditRepository.save(credit)).thenReturn(credit);
    when(creditMapper.toData(credit)).thenReturn(creditDto);
    CreditDto creditResponseDto = creditService.updateStatus(creditRequestDto);

    assertEquals(creditDto, creditResponseDto);
    verify(creditRepository, times(1)).save(credit);
  }

  @DisplayName("Test method to delete one credit")
  @Test
  void shouldDeleteOneByIdCredit() {
    Credit credit = Credit.builder().id(1L).build();

    when(creditRepository.findById(1L)).thenReturn(Optional.of(credit));
    doNothing().when(creditRepository).deleteById(1L);
    creditService.delete(1L);

    verify(creditRepository, times(1)).findById(1L);
    verify(creditRepository, times(1)).deleteById(1L);
  }
}
