package com.pichincha.test.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pichincha.credito.automotriz.domain.Brand;
import com.pichincha.credito.automotriz.domain.CarPark;
import com.pichincha.credito.automotriz.domain.Vehicle;
import com.pichincha.credito.automotriz.repository.VehicleRepository;
import com.pichincha.credito.automotriz.service.dto.BrandDto;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import com.pichincha.credito.automotriz.service.impl.VehicleServiceImpl;
import com.pichincha.credito.automotriz.service.mapper.VehicleMapper;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class VehicleServiceTest {

  @InjectMocks
  private VehicleServiceImpl vehicleService;
  @Mock
  private VehicleRepository vehicleRepository;
  @Spy
  private VehicleMapper vehicleMapper = Mappers.getMapper(VehicleMapper.class);

  @DisplayName("Test method return to list of vehicle")
  @Test
  void shouldReturnListOfVehicle() {
    List<Vehicle> vehicles = List.of(Vehicle.builder().build(),
        Vehicle.builder().build());

    when(vehicleRepository.findAll()).thenReturn(vehicles);
    when(vehicleMapper.toData(vehicles.get(0))).thenReturn(VehicleDto.builder().build());
    List<VehicleDto> vehiclesDto = vehicleService.getAll();

    assertEquals(2, vehiclesDto.size());
    verify(vehicleRepository, times(1)).findAll();
  }

  @DisplayName("Test method return one vehicle")
  @Test
  void shouldReturnOneByIdVehicle() {
    Vehicle vehicle = Vehicle.builder().build();

    when(vehicleRepository.findById(1L)).thenReturn(Optional.of(vehicle));
    when(vehicleMapper.toData(vehicle)).thenReturn(VehicleDto.builder().build());
    VehicleDto vehicleDto = vehicleService.getById(1L);

    assertEquals(vehicleDto, VehicleDto.builder().build());
  }

  @DisplayName("Test method to create new vehicle")
  @Test
  void givenVehicleWhenCreateVehicleThenCreateNewVehicle() {
    Vehicle vehicle = Vehicle.builder()
        .brand(Brand.builder().build())
        .carPark(CarPark.builder().build())
        .build();
    VehicleDto vehicleDtoRequest = VehicleDto.builder()
        .brandDto(BrandDto.builder().build())
        .carParkDto(CarParkDto.builder().build())
        .build();

    when(vehicleRepository.save(vehicle)).thenReturn(vehicle);
    when(vehicleMapper.toEntity(vehicleDtoRequest)).thenReturn(vehicle);
    when(vehicleMapper.toData(vehicle)).thenReturn(vehicleDtoRequest);
    VehicleDto vehicleDto = vehicleService.save(vehicleDtoRequest);

    assertEquals(vehicleDto, vehicleDtoRequest);
    verify(vehicleRepository, times(1)).save(vehicle);
  }

  @DisplayName("Test method to update one vehicle")
  @Test
  void givenVehicleWhenUpdateVehicleThenUpdateVehicle() {
    Vehicle vehicle = Vehicle.builder().id(1L).build();
    VehicleDto vehicleDtoRequest = VehicleDto.builder().id(1L).build();

    when(vehicleRepository.findById(1L)).thenReturn(Optional.of(vehicle));
    when(vehicleRepository.save(vehicle)).thenReturn(vehicle);
    when(vehicleMapper.toEntity(vehicleDtoRequest)).thenReturn(vehicle);
    when(vehicleMapper.toData(vehicle)).thenReturn(vehicleDtoRequest);
    VehicleDto vehicleDto = vehicleService.update(vehicleDtoRequest);

    assertEquals(vehicleDto, vehicleDtoRequest);
    verify(vehicleRepository, times(1)).save(vehicle);
  }

  @DisplayName("Test method to delete one vehicle")
  @Test
  void shouldDeleteOneByIdVehicle() {
    Vehicle vehicle = Vehicle.builder().id(1L).build();

    when(vehicleRepository.findById(1L)).thenReturn(Optional.of(vehicle));
    doNothing().when(vehicleRepository).deleteById(1L);
    vehicleService.delete(1L);

    verify(vehicleRepository, times(1)).findById(1L);
    verify(vehicleRepository, times(1)).deleteById(1L);
  }
}
