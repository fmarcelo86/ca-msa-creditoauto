INSERT INTO CLIENT VALUES(1, '1000000', 'Nombre 1', 30, '2008-09-22', 'Apellido 1', 'Calle 50', '314-25345', 'Soltero', '0000001', 'Pareja 1', FALSE);
INSERT INTO CLIENT VALUES(2, '2000000', 'Nombre 2', 30, '2008-09-22', 'Apellido 2', 'Calle 50', '314-25345', 'Soltero', '0000002', 'Pareja 2', FALSE);
INSERT INTO CLIENT VALUES(3, '3000000', 'Nombre 3', 30, '2008-09-22', 'Apellido 3', 'Calle 50', '314-25345', 'Soltero', '0000003', 'Pareja 3', FALSE);
INSERT INTO CLIENT VALUES(4, '4000000', 'Nombre 4', 30, '2008-09-22', 'Apellido 4', 'Calle 50', '314-25345', 'Soltero', '0000004', 'Pareja 4', FALSE);
INSERT INTO CLIENT VALUES(5, '5000000', 'Nombre 5', 30, '2008-09-22', 'Apellido 5', 'Calle 50', '314-25345', 'Soltero', '0000005', 'Pareja 5', FALSE);

INSERT INTO CLIENT VALUES(6, '6000000', 'Nombre 6', 30, '2008-09-22', 'Apellido 6', 'Calle 50', '314-25345', 'Soltero', '0000006', 'Pareja 6', TRUE);
INSERT INTO CLIENT VALUES(7, '7000000', 'Nombre 7', 30, '2008-09-22', 'Apellido 7', 'Calle 50', '314-25345', 'Soltero', '0000007', 'Pareja 7', TRUE);
INSERT INTO CLIENT VALUES(8, '8000000', 'Nombre 8', 30, '2008-09-22', 'Apellido 8', 'Calle 50', '314-25345', 'Soltero', '0000008', 'Pareja 8', TRUE);
INSERT INTO CLIENT VALUES(9, '9000000', 'Nombre 9', 30, '2008-09-22', 'Apellido 9', 'Calle 50', '314-25345', 'Soltero', '0000009', 'Pareja 9', TRUE);
INSERT INTO CLIENT VALUES(10, '10000000', 'Nombre 10', 30, '2008-09-22', 'Apellido 10', 'Calle 50', '314-25345', 'Soltero', '0000010', 'Pareja 10', TRUE);

INSERT INTO CLIENT VALUES(11, '11000000', 'Nombre 11', 30, '2008-09-22', 'Apellido 11', 'Calle 50', '314-25345', 'Soltero', '0000011', 'Pareja 11', TRUE);
INSERT INTO CLIENT VALUES(12, '12000000', 'Nombre 12', 30, '2008-09-22', 'Apellido 12', 'Calle 50', '314-25345', 'Soltero', '0000012', 'Pareja 12', TRUE);
INSERT INTO CLIENT VALUES(13, '13000000', 'Nombre 13', 30, '2008-09-22', 'Apellido 13', 'Calle 50', '314-25345', 'Soltero', '0000013', 'Pareja 13', TRUE);
INSERT INTO CLIENT VALUES(14, '14000000', 'Nombre 14', 30, '2008-09-22', 'Apellido 14', 'Calle 50', '314-25345', 'Soltero', '0000014', 'Pareja 14', TRUE);
INSERT INTO CLIENT VALUES(15, '15000000', 'Nombre 15', 30, '2008-09-22', 'Apellido 15', 'Calle 50', '314-25345', 'Soltero', '0000015', 'Pareja 15', TRUE);

INSERT INTO CLIENT VALUES(16, '11000000', 'Nombre 16', 30, '2008-09-22', 'Apellido 16', 'Calle 50', '314-25345', 'Soltero', '0000016', 'Pareja 16', TRUE);
INSERT INTO CLIENT VALUES(17, '12000000', 'Nombre 17', 30, '2008-09-22', 'Apellido 17', 'Calle 50', '314-25345', 'Soltero', '0000017', 'Pareja 17', TRUE);
INSERT INTO CLIENT VALUES(18, '13000000', 'Nombre 18', 30, '2008-09-22', 'Apellido 18', 'Calle 50', '314-25345', 'Soltero', '0000018', 'Pareja 18', TRUE);
INSERT INTO CLIENT VALUES(19, '14000000', 'Nombre 19', 30, '2008-09-22', 'Apellido 19', 'Calle 50', '314-25345', 'Soltero', '0000019', 'Pareja 19', TRUE);
INSERT INTO CLIENT VALUES(20, '15000000', 'Nombre 20', 30, '2008-09-22', 'Apellido 20', 'Calle 50', '314-25345', 'Soltero', '0000020', 'Pareja 20', TRUE);

INSERT INTO BRAND VALUES(1, 'Chevrolet');
INSERT INTO BRAND VALUES(2, 'KIA');
INSERT INTO BRAND VALUES(3, 'Hyundai');
INSERT INTO BRAND VALUES(4, 'Toyota');
INSERT INTO BRAND VALUES(5, 'Great Wall');
INSERT INTO BRAND VALUES(6, 'JAC');
INSERT INTO BRAND VALUES(7, 'Chery');
INSERT INTO BRAND VALUES(8, 'Renault');

INSERT INTO CAR_PARK VALUES(1000, 'Nombre 1', 'Calle 50', '314-25345', 3);

INSERT INTO SELLER VALUES(1, '1000000', 'Nombre 1', 'Apellido 1', 'Calle 50', '314-25345', '0000001', 1000, 30);
INSERT INTO SELLER VALUES(2, '2000000', 'Nombre 2', 'Apellido 2', 'Calle 50', '314-25345', '0000002', 1000, 30);
INSERT INTO SELLER VALUES(3, '3000000', 'Nombre 3', 'Apellido 3', 'Calle 50', '314-25345', '0000003', 1000, 30);
INSERT INTO SELLER VALUES(4, '4000000', 'Nombre 4', 'Apellido 4', 'Calle 50', '314-25345', '0000004', 1000, 35);
INSERT INTO SELLER VALUES(5, '5000000', 'Nombre 5', 'Apellido 5', 'Calle 50', '314-25345', '0000005', 1000, 25);

INSERT INTO SELLER VALUES(6, '6000000', 'Nombre 6', 'Apellido 6', 'Calle 50', '314-25345', '0000006', 1000, 30);
INSERT INTO SELLER VALUES(7, '7000000', 'Nombre 7', 'Apellido 7', 'Calle 50', '314-25345', '0000007', 1000, 30);
INSERT INTO SELLER VALUES(8, '8000000', 'Nombre 8', 'Apellido 8', 'Calle 50', '314-25345', '0000008', 1000, 30);
INSERT INTO SELLER VALUES(9, '9000000', 'Nombre 9', 'Apellido 9', 'Calle 50', '314-25345', '0000009', 1000, 30);
INSERT INTO SELLER VALUES(10, '10000000', 'Nombre 10', 'Apellido 10', 'Calle 50', '314-25345', '0000010', 1000, 30);

INSERT INTO SELLER VALUES(11, '11000000', 'Nombre 11', 'Apellido 11', 'Calle 50', '314-25345', '0000011', 1000, 30);
INSERT INTO SELLER VALUES(12, '12000000', 'Nombre 12', 'Apellido 12', 'Calle 50', '314-25345', '0000012', 1000, 30);
INSERT INTO SELLER VALUES(13, '13000000', 'Nombre 13', 'Apellido 13', 'Calle 50', '314-25345', '0000013', 1000, 30);
INSERT INTO SELLER VALUES(14, '14000000', 'Nombre 14', 'Apellido 14', 'Calle 50', '314-25345', '0000014', 1000, 30);
INSERT INTO SELLER VALUES(15, '15000000', 'Nombre 15', 'Apellido 15', 'Calle 50', '314-25345', '0000015', 1000, 30);

INSERT INTO SELLER VALUES(16, '11000000', 'Nombre 16', 'Apellido 16', 'Calle 50', '314-25345', '0000016', 1000, 30);
INSERT INTO SELLER VALUES(17, '12000000', 'Nombre 17', 'Apellido 17', 'Calle 50', '314-25345', '0000017', 1000, 30);
INSERT INTO SELLER VALUES(18, '13000000', 'Nombre 18', 'Apellido 18', 'Calle 50', '314-25345', '0000018', 1000, 30);
INSERT INTO SELLER VALUES(19, '14000000', 'Nombre 19', 'Apellido 19', 'Calle 50', '314-25345', '0000019', 1000, 30);
INSERT INTO SELLER VALUES(20, '15000000', 'Nombre 20', 'Apellido 20', 'Calle 50', '314-25345', '0000020', 1000, 30);