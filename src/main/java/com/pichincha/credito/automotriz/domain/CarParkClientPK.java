package com.pichincha.credito.automotriz.domain;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name CarParkClientPK.java
 * @class_description Composite Primary Key
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
@Embeddable
public class CarParkClientPK implements Serializable {

  @Column(name = "car_park_id")
  Long carParkId;
  @Column(name = "client_id")
  Long clientId;
}