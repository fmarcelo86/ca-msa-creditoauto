package com.pichincha.credito.automotriz.domain;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name Seller.java
 * @class_description Entity ORM Seller
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@FieldDefaults(level = PRIVATE)
@Entity
@Table(name = "seller")
public class Seller implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  String identification;
  String names;
  String lastNames;
  String address;
  String phone;
  String cellPhone;
  @OneToOne(optional = false)
  CarPark carPark;
  Integer age;
}
