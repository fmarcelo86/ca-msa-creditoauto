package com.pichincha.credito.automotriz.domain.enums;

public enum CreditStatusEnum {
  REGISTRADA, DESPACHADA, CANCELADA
}
