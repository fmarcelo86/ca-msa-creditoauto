package com.pichincha.credito.automotriz.domain;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name Vehicle.java
 * @class_description Entity ORM Vehicle
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@FieldDefaults(level = PRIVATE)
@Entity
@Table(name = "vehicle")
public class Vehicle implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  @Column(unique = true)
  String plate;
  String model;
  @Column(name = "chassis_number")
  String chassisNumber;
  @ManyToOne(optional = false)
  Brand brand;
  String type;
  String cylinder;
  Double price;
  @OneToOne(optional = false)
  CarPark carPark;
}
