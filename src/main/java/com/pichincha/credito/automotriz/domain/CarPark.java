package com.pichincha.credito.automotriz.domain;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @class_name CarPark.java
 * @class_description Entity ORM CarPark
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@FieldDefaults(level = PRIVATE)
@Entity
@Table(name = "car_park")
public class CarPark implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  String name;
  String address;
  String phone;
  Long pointSaleNumber;
  @OneToMany(mappedBy = "client")
  List<CarParkClient> carParkClients;
}
