package com.pichincha.credito.automotriz.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @class_name ErrorResponse.java
 * @class_description Entity for error response of service rest
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@EqualsAndHashCode
@Builder
public class ErrorResponse {
    private String code;
    private String message;
}