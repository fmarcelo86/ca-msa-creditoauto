package com.pichincha.credito.automotriz.domain;

import static javax.persistence.CascadeType.ALL;
import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @class_name CarParkClient.java
 * @class_description Entity ORM CarParkClient
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@FieldDefaults(level = PRIVATE)
@Entity
@Table
public class CarParkClient implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Embedded
  private CarParkClientPK fk;

  @ManyToOne(cascade = {ALL})
  @MapsId("car_park_id")
  @JoinColumn(name = "car_park_id")
  private CarPark carPark;

  @ManyToOne(cascade = {ALL})
  @MapsId("client_id")
  @JoinColumn(name = "client_id")
  private Client client;

  @UpdateTimestamp
  @Temporal(TemporalType.DATE)
  Date assignmentDate;
}
