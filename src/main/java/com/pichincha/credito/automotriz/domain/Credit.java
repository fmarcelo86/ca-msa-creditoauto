package com.pichincha.credito.automotriz.domain;

import static lombok.AccessLevel.PRIVATE;

import com.pichincha.credito.automotriz.domain.enums.CreditStatusEnum;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @class_name Credit.java
 * @class_description Entity ORM Credit
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@FieldDefaults(level = PRIVATE)
@DynamicUpdate
@Entity
@Table(name = "credit")
@EntityListeners(AuditingEntityListener.class)
public class Credit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  @Temporal(TemporalType.DATE)
  @CreatedDate
  Date createdDate;
  @ManyToOne(optional = false)
  Client client;
  @ManyToOne(optional = false)
  CarPark carPark;
  @ManyToOne(optional = false)
  Vehicle vehicle;
  Integer monthsTerm;
  Integer quotas;
  Double initialQuota;
  @ManyToOne(optional = false)
  Seller seller;
  @Enumerated(EnumType.STRING)
  CreditStatusEnum status;
  String observation;
}
