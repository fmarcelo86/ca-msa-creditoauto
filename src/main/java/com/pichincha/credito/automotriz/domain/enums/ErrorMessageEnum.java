package com.pichincha.credito.automotriz.domain.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ErrorMessageEnum {
    CAR_PARK_NOT_FOUND("PBB0001", "CarPark Not Found"),
    VEHICLE_NOT_FOUND("PBB0002", "Vehicle Not Found"),
    CAR_PAR_CLIENT_NOT_FOUND("PBB0003", "CarPark or Client Not Found"),
    CREDIT_NOT_FOUND("PBB0004", "Credit Not Found"),
    CREDIT_CLIENT_NOT_VALID("PBB0005", "The customer has an active request today"),
    CREDIT_VEHICLE_NOT_VALID("PBB0006", "The vehicle has an active request"),
    CREDIT_CLIENT_NOT_ACTIVE("PBB0007", "The client is not subject credit");

    private final String code;
    private final String message;
}