package com.pichincha.credito.automotriz.domain;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name Client.java
 * @class_description Entity ORM Client
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@FieldDefaults(level = PRIVATE)
@Entity
@Table(name = "client")
public class Client implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  String identification;
  String names;
  Integer age;
  @Temporal(TemporalType.DATE)
  Date birthDate;
  String lastNames;
  String address;
  String phone;
  String civilStatus;
  String spouseId;
  String spouseName;
  Boolean subjectCredit;
  @OneToMany(mappedBy = "carPark")
  List<CarParkClient> carParkClients;
}
