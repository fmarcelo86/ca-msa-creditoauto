package com.pichincha.credito.automotriz.util;

import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.exception.ServiceNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Log4j2
@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(ServiceNotFoundException.class)
  public ResponseEntity<ErrorResponse> handleExceptions(ServiceNotFoundException exception) {
    log.error("Error", exception);
    ErrorResponse response = ErrorResponse.builder()
        .code(exception.getErrorMessageEnum().getCode())
        .message(exception.getErrorMessageEnum().getMessage())
        .build();
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorResponse> handleExceptions(MethodArgumentNotValidException ex) {
    List<String> errors = ex.getBindingResult().getFieldErrors()
        .stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.toList());

    ErrorResponse response = ErrorResponse.builder()
        .message(errors.toString())
        .build();
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }
}