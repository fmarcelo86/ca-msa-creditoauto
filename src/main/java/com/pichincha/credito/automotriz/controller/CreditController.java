package com.pichincha.credito.automotriz.controller;

import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.domain.enums.CreditStatusEnum;
import com.pichincha.credito.automotriz.service.CreditService;
import com.pichincha.credito.automotriz.service.dto.CreditDto;
import com.pichincha.credito.automotriz.service.dto.CreditRequestDto;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fromanan@pichincha.com
 * @class_name CreditController.java
 * @class_description Controller for manage the request of Credit
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/credit")
public class CreditController {

  private final CreditService creditService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CreditDto>> getAll() {
    return ResponseEntity.ok(creditService.getAll());
  }

  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getById(@PathVariable long id) {
    return ResponseEntity.ok(creditService.getById(id));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> create(@RequestBody @Valid CreditDto creditDto) {
    creditDto.setStatus(CreditStatusEnum.REGISTRADA);
    CreditDto creditDtoResponse = creditService.save(creditDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(creditDtoResponse);
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@RequestBody @Valid CreditDto creditDto) {
    CreditDto creditDtoResponse = creditService.update(creditDto);
    return ResponseEntity.ok(creditDtoResponse);
  }

  @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@RequestBody @Valid CreditRequestDto creditDto) {
    CreditDto creditDtoResponse = creditService.updateStatus(creditDto);
    return ResponseEntity.ok(creditDtoResponse);
  }

  @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ErrorResponse> delete(@PathVariable long id) {
    creditService.delete(id);
    return ResponseEntity.ok().build();
  }
}
