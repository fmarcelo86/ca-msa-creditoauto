package com.pichincha.credito.automotriz.controller;

import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.CarParkClientService;
import com.pichincha.credito.automotriz.service.dto.CarParkClientDto;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fromanan@pichincha.com
 * @class_name CarParkClientController.java
 * @class_description Controller for assignment of client in CarPark
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/assignment")
public class CarParkClientController {

  private final CarParkClientService carParkClientService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CarParkClientDto>> getAll() {
    return ResponseEntity.ok(carParkClientService.getAll());
  }

  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getById(@PathVariable long id) {
    return ResponseEntity.ok(carParkClientService.getById(id));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CarParkClientDto> create(
      @RequestBody @Valid CarParkClientDto carParkClientDto) {
    CarParkClientDto carParkClientDtoResponse = carParkClientService.save(carParkClientDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(carParkClientDtoResponse);
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@RequestBody @Valid CarParkClientDto carParkClientDto) {
    CarParkClientDto carParkClientDtoResponse = carParkClientService.update(carParkClientDto);
    return ResponseEntity.ok(carParkClientDtoResponse);
  }

  @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ErrorResponse> delete(@PathVariable long id) {
    carParkClientService.delete(id);
    return ResponseEntity.ok().build();
  }
}
