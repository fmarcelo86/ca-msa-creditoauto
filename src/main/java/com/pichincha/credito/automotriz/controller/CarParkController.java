package com.pichincha.credito.automotriz.controller;

import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.CarParkService;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fromanan@pichincha.com
 * @class_name CarParkController.java
 * @class_description Controller for manage the request of CarPark
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/carpark")
public class CarParkController {

  private final CarParkService carParkService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CarParkDto>> getAll() {
    return ResponseEntity.ok(carParkService.getAll());
  }

  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getById(@PathVariable long id) {
    return ResponseEntity.ok(carParkService.getById(id));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CarParkDto> create(@RequestBody @Valid CarParkDto carParkDto) {
    CarParkDto carParkDtoResponse = carParkService.save(carParkDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(carParkDtoResponse);
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@RequestBody @Valid CarParkDto carParkDto) {
    CarParkDto carParkDtoResponse = carParkService.update(carParkDto);
    return ResponseEntity.ok(carParkDtoResponse);
  }

  @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ErrorResponse> delete(@PathVariable long id) {
    carParkService.delete(id);
    return ResponseEntity.ok().build();
  }
}
