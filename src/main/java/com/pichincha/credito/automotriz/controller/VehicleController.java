package com.pichincha.credito.automotriz.controller;

import com.pichincha.credito.automotriz.domain.ErrorResponse;
import com.pichincha.credito.automotriz.service.VehicleService;
import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fromanan@pichincha.com
 * @class_name VehicleController.java
 * @class_description Controller for manage the request of Vehicle
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/vehicle")
public class VehicleController {

  private final VehicleService vehicleService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<VehicleDto>> getAll() {
    return ResponseEntity.ok(vehicleService.getAll());
  }

  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getById(@PathVariable long id) {
    return ResponseEntity.ok(vehicleService.getById(id));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<VehicleDto> create(@RequestBody @Valid VehicleDto vehicleDto) {
    VehicleDto vehicleDtoResponse = vehicleService.save(vehicleDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(vehicleDtoResponse);
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@RequestBody @Valid VehicleDto vehicleDto) {
    VehicleDto vehicleDtoResponse = vehicleService.update(vehicleDto);
    return ResponseEntity.ok(vehicleDtoResponse);
  }

  @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ErrorResponse> delete(@PathVariable long id) {
    vehicleService.delete(id);
    return ResponseEntity.ok().build();
  }
}
