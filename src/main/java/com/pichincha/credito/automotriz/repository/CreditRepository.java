package com.pichincha.credito.automotriz.repository;

import com.pichincha.credito.automotriz.domain.Credit;
import com.pichincha.credito.automotriz.domain.enums.CreditStatusEnum;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author fromanan@pichincha.com
 * @class_name CreditRepository.java
 * @class_description Manage the CRUD operation of Credit
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@Repository
public interface CreditRepository extends JpaRepository<Credit, Long> {

  Optional<Boolean> existsByClientIdAndCreatedDateAndStatus(Long id, Date createdDate,
      CreditStatusEnum status);

  Optional<Boolean> existsByVehicleIdAndStatus(Long id, CreditStatusEnum status);
}
