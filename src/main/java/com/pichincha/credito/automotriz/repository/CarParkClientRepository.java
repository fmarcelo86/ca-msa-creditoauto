package com.pichincha.credito.automotriz.repository;

import com.pichincha.credito.automotriz.domain.CarParkClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @class_name CarParkClientRepository.java
 * @class_description Manage the CRUD operation of CarParkClient
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Repository
public interface CarParkClientRepository extends JpaRepository<CarParkClient, Long> {

}
