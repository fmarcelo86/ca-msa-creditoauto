package com.pichincha.credito.automotriz.repository;

import com.pichincha.credito.automotriz.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @class_name VehicleRepository.java
 * @class_description Manage the CRUD operation of Vehicle
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

}
