package com.pichincha.credito.automotriz.repository;

import com.pichincha.credito.automotriz.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author fromanan@pichincha.com
 * @class_name ClientRepository.java
 * @class_description Manage the CRUD operation of Client
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

}
