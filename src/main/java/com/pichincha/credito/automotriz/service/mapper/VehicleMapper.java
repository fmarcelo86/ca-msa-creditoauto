package com.pichincha.credito.automotriz.service.mapper;

import com.pichincha.credito.automotriz.domain.Vehicle;
import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @class_name VehicleMapper.java
 * @class_description Manage the cross mapping between Vehicle and VehicleDto
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VehicleMapper {
    @Mapping(source = "carParkDto",  target = "carPark")
    @Mapping(source = "brandDto",  target = "brand")
    Vehicle toEntity(VehicleDto vehicleDto);
    @Mapping(source = "carPark",  target = "carParkDto")
    @Mapping(source = "brand",  target = "brandDto")
    VehicleDto toData(Vehicle vehicle);
}