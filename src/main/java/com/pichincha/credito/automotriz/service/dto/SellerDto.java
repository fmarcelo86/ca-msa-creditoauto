package com.pichincha.credito.automotriz.service.dto;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @class_name SellerDto.java
 * @class_description DTO of response service
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class SellerDto implements Serializable {

  Long id;
  String identification;
  String names;
  String lastNames;
  String address;
  String phone;
  String cellPhone;
  CarParkDto carParkDto;
  Integer age;
}
