package com.pichincha.credito.automotriz.service.dto;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name VehicleDto.java
 * @class_description DTO of response service
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class VehicleDto implements Serializable {
  @Setter
  Long id;
  @NotBlank
  String plate;
  @NotBlank
  String model;
  @NotBlank
  String chassisNumber;
  @NotNull
  BrandDto brandDto;
  String type;
  @NotBlank
  String cylinder;
  @Positive
  @NotNull
  Double price;
  @NotNull
  CarParkDto carParkDto;
}
