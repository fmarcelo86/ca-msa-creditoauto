package com.pichincha.credito.automotriz.service.mapper;

import com.pichincha.credito.automotriz.domain.CarPark;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @class_name CarParkMapper.java
 * @class_description Manage the cross mapping between CarPark and CarParkDto
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CarParkMapper {
    CarPark toEntity(CarParkDto carParkDto);

    CarParkDto toData(CarPark carPark);
}