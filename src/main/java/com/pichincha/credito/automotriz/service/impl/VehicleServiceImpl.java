package com.pichincha.credito.automotriz.service.impl;

import com.pichincha.credito.automotriz.domain.enums.ErrorMessageEnum;
import com.pichincha.credito.automotriz.exception.ServiceNotFoundException;
import com.pichincha.credito.automotriz.repository.VehicleRepository;
import com.pichincha.credito.automotriz.service.VehicleService;
import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import com.pichincha.credito.automotriz.service.mapper.VehicleMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @class_name VehicleServiceImpl.java
 * @class_description Manage the business logic and persistence of Vehicle
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

  private final VehicleRepository vehicleRepository;
  private final VehicleMapper vehicleMapper;

  @Override
  public List<VehicleDto> getAll() {
    return vehicleRepository.findAll()
        .stream()
        .map(vehicleMapper::toData)
        .collect(Collectors.toList());
  }

  @Override
  public VehicleDto getById(Long id) {
    return Optional.ofNullable(id)
        .flatMap(vehicleRepository::findById)
        .map(vehicleMapper::toData)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.VEHICLE_NOT_FOUND));
  }

  @Override
  public VehicleDto save(VehicleDto vehicleDto) {
    vehicleDto.setId(0L);
    return Optional.of(vehicleDto)
        .map(vehicleMapper::toEntity)
        .map(vehicleRepository::save)
        .map(vehicleMapper::toData)
        .orElseThrow();
  }

  @Override
  public VehicleDto update(VehicleDto vehicleDto) {
    vehicleRepository.findById(vehicleDto.getId())
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.VEHICLE_NOT_FOUND));

    return Optional.of(vehicleDto)
        .map(vehicleMapper::toEntity)
        .map(vehicleRepository::save)
        .map(vehicleMapper::toData)
        .orElseThrow();
  }

  @Override
  public void delete(Long id) {
    vehicleRepository.findById(id)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.VEHICLE_NOT_FOUND));
    vehicleRepository.deleteById(id);
  }
}
