package com.pichincha.credito.automotriz.service;

import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import java.util.List;

/**
 * @class_name CarParkService.java
 * @class_description Specification the operation of CarParkService
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
public interface CarParkService {
  List<CarParkDto> getAll();
  CarParkDto getById(Long id);
  CarParkDto save(CarParkDto carParkDto);
  CarParkDto update(CarParkDto carParkDto);
  void delete(Long id);
}
