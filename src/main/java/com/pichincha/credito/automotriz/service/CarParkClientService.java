package com.pichincha.credito.automotriz.service;

import com.pichincha.credito.automotriz.service.dto.CarParkClientDto;
import java.util.List;

/**
 * @class_name CarParkClientService.java
 * @class_description Specification the operation of CarParkClientService
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
public interface CarParkClientService {
  List<CarParkClientDto> getAll();
  CarParkClientDto getById(Long id);
  CarParkClientDto save(CarParkClientDto carParkClientDto);
  CarParkClientDto update(CarParkClientDto carParkClientDto);
  void delete(Long id);
}
