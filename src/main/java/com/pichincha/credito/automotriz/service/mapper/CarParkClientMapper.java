package com.pichincha.credito.automotriz.service.mapper;

import com.pichincha.credito.automotriz.domain.CarParkClient;
import com.pichincha.credito.automotriz.service.dto.CarParkClientDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @class_name CarParkClientMapper.java
 * @class_description Manage the cross mapping between CarParkClient and CarParkClientDto
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CarParkClientMapper {
    @Mapping(source = "carParkDto",  target = "carPark")
    @Mapping(source = "clientDto",  target = "client")
    CarParkClient toEntity(CarParkClientDto carParkClientDto);
    @Mapping(source = "carPark",  target = "carParkDto")
    @Mapping(source = "client",  target = "clientDto")
    CarParkClientDto toData(CarParkClient carParkClient);
}