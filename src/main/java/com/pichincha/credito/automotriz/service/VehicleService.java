package com.pichincha.credito.automotriz.service;

import com.pichincha.credito.automotriz.service.dto.VehicleDto;
import java.util.List;

/**
 * @class_name VehicleService.java
 * @class_description Specification the operation of VehicleService
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
public interface VehicleService {
  List<VehicleDto> getAll();
  VehicleDto getById(Long id);
  VehicleDto save(VehicleDto vehicleDto);
  VehicleDto update(VehicleDto vehicleDto);
  void delete(Long id);
}
