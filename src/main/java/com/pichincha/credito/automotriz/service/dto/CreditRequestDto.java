package com.pichincha.credito.automotriz.service.dto;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @author fromanan@pichincha.com
 * @class_name ClientDto.java
 * @class_description DTO of response service
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
public class CreditRequestDto implements Serializable {

  @Positive
  @NotNull
  Long id;
  @Setter
  @Pattern(regexp = "REGISTRADA|DESPACHADA|CANCELADA")
  String status;
}
