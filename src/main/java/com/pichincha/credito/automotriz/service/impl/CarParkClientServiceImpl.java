package com.pichincha.credito.automotriz.service.impl;

import com.pichincha.credito.automotriz.domain.enums.ErrorMessageEnum;
import com.pichincha.credito.automotriz.exception.ServiceNotFoundException;
import com.pichincha.credito.automotriz.repository.CarParkClientRepository;
import com.pichincha.credito.automotriz.service.CarParkClientService;
import com.pichincha.credito.automotriz.service.dto.CarParkClientDto;
import com.pichincha.credito.automotriz.service.mapper.CarParkClientMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @class_name CarParkClientServiceImpl.java
 * @class_description Manage the business logic and persistence of CarParkClient
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Service
@RequiredArgsConstructor
public class CarParkClientServiceImpl implements CarParkClientService {

  private final CarParkClientRepository carParkClientRepository;
  private final CarParkClientMapper carParkClientMapper;

  @Override
  public List<CarParkClientDto> getAll() {
    return carParkClientRepository.findAll()
        .stream()
        .map(carParkClientMapper::toData)
        .collect(Collectors.toList());
  }

  @Override
  public CarParkClientDto getById(Long id) {
    return Optional.ofNullable(id)
        .flatMap(carParkClientRepository::findById)
        .map(carParkClientMapper::toData)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CAR_PAR_CLIENT_NOT_FOUND));
  }

  @Override
  public CarParkClientDto save(CarParkClientDto carParkClientDto) {
    carParkClientDto.setId(0L);
    return Optional.of(carParkClientDto)
        .map(carParkClientMapper::toEntity)
        .map(carParkClientRepository::save)
        .map(carParkClientMapper::toData)
        .orElseThrow();
  }

  @Override
  public CarParkClientDto update(CarParkClientDto carParkClientDto) {
    carParkClientRepository.findById(carParkClientDto.getId())
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CAR_PAR_CLIENT_NOT_FOUND));

    return Optional.of(carParkClientDto)
        .map(carParkClientMapper::toEntity)
        .map(carParkClientRepository::save)
        .map(carParkClientMapper::toData)
        .orElseThrow();
  }

  @Override
  public void delete(Long id) {
    carParkClientRepository.findById(id)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CAR_PAR_CLIENT_NOT_FOUND));
    carParkClientRepository.deleteById(id);
  }
}
