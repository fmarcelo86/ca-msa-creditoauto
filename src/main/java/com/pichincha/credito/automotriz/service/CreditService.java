package com.pichincha.credito.automotriz.service;

import com.pichincha.credito.automotriz.service.dto.CreditDto;
import com.pichincha.credito.automotriz.service.dto.CreditRequestDto;
import java.util.List;

/**
 * @class_name CreditService.java
 * @class_description Specification the operation of CreditService
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
public interface CreditService {
  List<CreditDto> getAll();
  CreditDto getById(Long id);
  CreditDto save(CreditDto creditDto);
  CreditDto update(CreditDto creditDto);
  CreditDto updateStatus(CreditRequestDto creditDto);
  void delete(Long id);
}
