package com.pichincha.credito.automotriz.service.dto;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name CarParkDto.java
 * @class_description DTO of response service
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class CarParkDto implements Serializable {
    @Setter
    Long id;
    @NotBlank(message = "name is required not empty")
    String name;
    @NotBlank(message = "address is required not empty")
    String address;
    @NotBlank(message = "phone is required not empty")
    String phone;
    @Positive(message = "pointSaleNumber to be positive")
    @NotNull(message = "pointSaleNumber is required not null")
    Long pointSaleNumber;
}
