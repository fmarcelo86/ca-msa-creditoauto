package com.pichincha.credito.automotriz.service.mapper;

import com.pichincha.credito.automotriz.domain.Credit;
import com.pichincha.credito.automotriz.service.dto.CreditDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @class_name CreditMapper.java
 * @class_description Manage the cross mapping between Credit and CreditDto
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CreditMapper {
    @Mapping(source = "carParkDto",  target = "carPark")
    @Mapping(source = "clientDto",  target = "client")
    @Mapping(source = "sellerDto",  target = "seller")
    @Mapping(source = "vehicleDto",  target = "vehicle")
    Credit toEntity(CreditDto creditDto);
    @Mapping(source = "carPark",  target = "carParkDto")
    @Mapping(source = "client",  target = "clientDto")
    @Mapping(source = "seller",  target = "sellerDto")
    @Mapping(source = "vehicle",  target = "vehicleDto")
    CreditDto toData(Credit credit);
}