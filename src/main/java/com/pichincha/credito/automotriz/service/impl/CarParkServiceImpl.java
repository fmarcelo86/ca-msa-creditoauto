package com.pichincha.credito.automotriz.service.impl;

import com.pichincha.credito.automotriz.domain.enums.ErrorMessageEnum;
import com.pichincha.credito.automotriz.exception.ServiceNotFoundException;
import com.pichincha.credito.automotriz.repository.CarParkRepository;
import com.pichincha.credito.automotriz.service.CarParkService;
import com.pichincha.credito.automotriz.service.dto.CarParkDto;
import com.pichincha.credito.automotriz.service.mapper.CarParkMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @class_name CarParkServiceImpl.java
 * @class_description Manage the business logic and persistence of CarPark
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Service
@RequiredArgsConstructor
public class CarParkServiceImpl implements CarParkService {

  private final CarParkRepository carParkRepository;
  private final CarParkMapper carParkMapper;

  @Override
  public List<CarParkDto> getAll() {
    return carParkRepository.findAll()
        .stream()
        .map(carParkMapper::toData)
        .collect(Collectors.toList());
  }

  @Override
  public CarParkDto getById(Long id) {
    return Optional.ofNullable(id)
        .flatMap(carParkRepository::findById)
        .map(carParkMapper::toData)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CAR_PARK_NOT_FOUND));
  }

  @Override
  public CarParkDto save(CarParkDto carParkDto) {
    carParkDto.setId(0L);
    return Optional.of(carParkDto)
        .map(carParkMapper::toEntity)
        .map(carParkRepository::save)
        .map(carParkMapper::toData)
        .orElseThrow();
  }

  @Override
  public CarParkDto update(CarParkDto carParkDto) {
    carParkRepository.findById(carParkDto.getId())
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CAR_PARK_NOT_FOUND));

    return Optional.of(carParkDto)
        .map(carParkMapper::toEntity)
        .map(carParkRepository::save)
        .map(carParkMapper::toData)
        .orElseThrow();
  }

  @Override
  public void delete(Long id) {
    carParkRepository.findById(id)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CAR_PARK_NOT_FOUND));
    carParkRepository.deleteById(id);
  }
}
