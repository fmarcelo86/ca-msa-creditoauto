package com.pichincha.credito.automotriz.service.dto;

import static lombok.AccessLevel.PRIVATE;

import com.pichincha.credito.automotriz.domain.enums.CreditStatusEnum;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @class_name ClientDto.java
 * @class_description DTO of response service
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class CreditDto implements Serializable {

  Long id;
  Date createdDate;
  @NotNull
  ClientDto clientDto;
  @NotNull
  CarParkDto carParkDto;
  @NotNull
  VehicleDto vehicleDto;
  @Positive
  @NotNull
  Integer monthsTerm;
  @Positive
  @NotNull
  Integer quotas;
  @Positive
  @NotNull
  Double initialQuota;
  @NotNull
  SellerDto sellerDto;
  CreditStatusEnum status;
  String observation;
}
