package com.pichincha.credito.automotriz.service.impl;

import com.pichincha.credito.automotriz.domain.CarParkClient;
import com.pichincha.credito.automotriz.domain.CarParkClientPK;
import com.pichincha.credito.automotriz.domain.Client;
import com.pichincha.credito.automotriz.domain.Credit;
import com.pichincha.credito.automotriz.domain.enums.CreditStatusEnum;
import com.pichincha.credito.automotriz.domain.enums.ErrorMessageEnum;
import com.pichincha.credito.automotriz.exception.ServiceNotFoundException;
import com.pichincha.credito.automotriz.repository.CarParkClientRepository;
import com.pichincha.credito.automotriz.repository.ClientRepository;
import com.pichincha.credito.automotriz.repository.CreditRepository;
import com.pichincha.credito.automotriz.service.CreditService;
import com.pichincha.credito.automotriz.service.dto.CreditDto;
import com.pichincha.credito.automotriz.service.dto.CreditRequestDto;
import com.pichincha.credito.automotriz.service.mapper.CreditMapper;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author fromanan@pichincha.com
 * @class_name CreditServiceImpl.java
 * @class_description Manage the business logic and persistence of Credit
 * @create_date 18/05/2023 Copyright 2023 Banco Pichincha.
 */
@Service
@RequiredArgsConstructor
public class CreditServiceImpl implements CreditService {

  private final CreditRepository creditRepository;
  private final ClientRepository clientRepository;
  private final CarParkClientRepository carParkClientRepository;
  private final CreditMapper creditMapper;

  @Override
  public List<CreditDto> getAll() {
    return creditRepository.findAll()
        .stream()
        .map(creditMapper::toData)
        .collect(Collectors.toList());
  }

  @Override
  public CreditDto getById(Long id) {
    return Optional.ofNullable(id)
        .flatMap(creditRepository::findById)
        .map(creditMapper::toData)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_NOT_FOUND));
  }

  @Transactional
  @Override
  public CreditDto save(CreditDto creditDto) {
    CarParkClientPK parkClientPK = CarParkClientPK.builder()
        .clientId(creditDto.getClientDto().getId())
        .carParkId(creditDto.getCarParkDto().getId()).build();
    CarParkClient carParkClient = CarParkClient.builder().fk(parkClientPK).build();

    clientRepository.findById(creditDto.getClientDto().getId())
        .map(Client::getSubjectCredit).filter(exists -> exists)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_CLIENT_NOT_ACTIVE));

    creditRepository.existsByClientIdAndCreatedDateAndStatus(creditDto.getClientDto().getId(),
            Calendar.getInstance().getTime(), CreditStatusEnum.REGISTRADA).filter(exists -> !exists)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_CLIENT_NOT_VALID));

    creditRepository.existsByVehicleIdAndStatus(creditDto.getVehicleDto().getId(),
            CreditStatusEnum.REGISTRADA).filter(exists -> !exists)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_VEHICLE_NOT_VALID));

    carParkClientRepository.save(carParkClient);
    creditDto.setId(0L);
    return Optional.of(creditDto).map(creditMapper::toEntity).map(creditRepository::save)
        .map(creditMapper::toData).orElseThrow();
  }

  @Override
  public CreditDto update(CreditDto creditDto) {
    creditRepository.findById(creditDto.getId())
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_NOT_FOUND));

    return Optional.of(creditDto)
        .map(creditMapper::toEntity)
        .map(creditRepository::save)
        .map(creditMapper::toData)
        .orElseThrow();
  }

  @Override
  public CreditDto updateStatus(CreditRequestDto creditDto) {
    Credit credit = creditRepository.findById(creditDto.getId())
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_NOT_FOUND));
    credit.setStatus(CreditStatusEnum.valueOf(creditDto.getStatus()));

    return Optional.of(credit)
        .map(creditRepository::save)
        .map(creditMapper::toData)
        .orElseThrow();
  }

  @Override
  public void delete(Long id) {
    creditRepository.findById(id)
        .orElseThrow(() -> new ServiceNotFoundException(ErrorMessageEnum.CREDIT_NOT_FOUND));
    creditRepository.deleteById(id);
  }
}
