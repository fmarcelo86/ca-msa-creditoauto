package com.pichincha.credito.automotriz.exception;

import com.pichincha.credito.automotriz.domain.enums.ErrorMessageEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @class_name ServiceNotFoundException.java
 * @class_description Manage the exceptions owns
 * @author fromanan@pichincha.com
 * @create_date 18/05/2023
 * Copyright 2023 Banco Pichincha.
 */
@Getter
@RequiredArgsConstructor
public class ServiceNotFoundException extends RuntimeException {
  private final ErrorMessageEnum errorMessageEnum;
}
